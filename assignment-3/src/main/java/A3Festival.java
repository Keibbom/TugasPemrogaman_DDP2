import javari.animal.*;
import javari.park.Attraction;
import javari.park.Visitor;
import javari.reader.*;
import javari.park.Registration;
import javari.park.SelectedAttraction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;


public class A3Festival {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println("\n... Opening default section database from data. ... " +
                "File not found or incorrect file!" );

        CsvReader attractionCsv;
        CsvReader categoriesCsv;
        CsvReader sectionCsv;
        CsvReader recordCsv;
        Path destination;
        while (true) {
            System.out.print("\nPlease provide the source data path: ");
            String defPath = input.nextLine();

            File folder = new File(defPath);
            destination = Paths.get(defPath);
            String attractionString = "";
            String categoriesString = "";
            String recordString = "";
            try {
                for (File fileEntry: Objects.requireNonNull(folder.listFiles())) {
                    if (fileEntry.getName().contains("animals_attractions.csv")) {
                        attractionString = defPath + "/" + fileEntry.getName();
                    } else if (fileEntry.getName().contains("animals_categories.csv")) {
                        categoriesString = defPath + "/" + fileEntry.getName();
                    } else if (fileEntry.getName().contains("animals_records.csv")) {
                        recordString = defPath + "/" + fileEntry.getName();
                    }
                }
                Path attractionPath = Paths.get(attractionString);
                Path categoriesPath = Paths.get(categoriesString);
                Path recordPath = Paths.get(recordString);
                attractionCsv = new AttractionReader(attractionPath);
                sectionCsv = new SectionReader(categoriesPath);
                categoriesCsv = new CategoriesReader(categoriesPath);
                recordCsv = new RecordReader(recordPath);
                System.out.println("... Loading... Success... System is populating data...");
                break;
            } catch (IOException | NullPointerException e) {
                System.out.println("... Loading... Fail... Data is not valid...");
            }
        }

        System.out.println("Found " + sectionCsv.countValidRecords() + " valid sections and "
                + sectionCsv.countInvalidRecords() + " invalid sections");
        System.out.println("Found " + attractionCsv.countValidRecords() + " valid attractions and "
                + attractionCsv.countInvalidRecords() + " invalid attractions");
        System.out.println("Found " + categoriesCsv.countValidRecords() +
                " valid animal categories and " + categoriesCsv.countInvalidRecords()
                + " invalid animal categories");
        System.out.println("Found " + recordCsv.countValidRecords() + " valid animal records and "
                + recordCsv.countInvalidRecords() + " invalid animal records");

        ArrayList<ArrayList<ArrayList<Animal>>> listAnimal =
                arrangeAnimal(((RecordReader) recordCsv).getAnimalList());

        System.out.println("\nWelcome to Javari Park Festival - Registration Service!");
        System.out.println("\nPlease answer the questions by typing the number. " +
                "Type # if you want to return to the previous menu");

        String[][][] listAttraction = ((AttractionReader) attractionCsv).getListAttraction();
        System.out.println(listAttraction[0][0][0]);
        String[][] listCategories = ((CategoriesReader) categoriesCsv).getListCategories();
        String[] listSection = ((SectionReader) sectionCsv).getListSection();

        ArrayList<SelectedAttraction> selectedAttractions = new ArrayList<>();
        String name;
        loop: while (true) {
            String attractionName = "";
            String animalType = "";
            List<Animal> animalList= new ArrayList<>();

            System.out.println("\nJavari Park has 3 sections:");
            for (int i = 0; i < listSection.length; i++) {
                System.out.println(i + 1 + ". " + listSection[i]);
            }
            System.out.print("Please choose your preferred section (type the number): ");

            String command1 = input.nextLine();
            int typePick = 0;
            int secPick;
            try {
                secPick = Integer.parseInt(command1) - 1;

                if (secPick >= 0 && secPick < listSection.length) {
                    if (listSection[secPick] != null) {
                        insideLoop1: while (true) {
                            System.out.println("\n--" + listSection[secPick] + "--");
                            for (int i = 0; i < listCategories[secPick].length; i++) {
                                System.out.println(i + 1 + ". " + listCategories[secPick][i]);
                            }
                            System.out.print("Please choose your preferred animals "
                                    + "(type the number): ");

                            String command2 = input.nextLine();
                            try {
                                typePick = Integer.parseInt(command2) - 1;

                                if (listAnimal.get(secPick).get(typePick).size() != 0) {
                                    boolean allCannotPerform = true;
                                    for (Animal animal: listAnimal.get(secPick).get(typePick)) {
                                        if (animal.isShowable()) {
                                            allCannotPerform = false;
                                        }
                                    }
                                    if (allCannotPerform) {
                                        System.out.println("Unfortunately, no "
                                                + listCategories[secPick][typePick]
                                                + " can perform any attraction, "
                                                + "please choose other animals");
                                        continue;
                                    }
                                    while (true) {
                                        animalType = listCategories[secPick][typePick];
                                        System.out.println("\n---" + animalType + "---");
                                        System.out.println("Attractions by " + animalType +":");
                                        String[] attractions = listAttraction[secPick][typePick];
                                        for (int i = 0; i < attractions.length;i++) {
                                            System.out.println(i + 1 + ". " + attractions[i]);
                                        }
                                        System.out.print("Please choose your preferred "
                                                + "attractions (type the number): ");

                                        String command3 = input.nextLine();
                                        try {
                                            int attrPick = Integer.parseInt(command3) - 1;

                                            if (attrPick >= 0 && attrPick < attractions.length) {
                                                String attraction = listAttraction
                                                        [secPick][typePick][attrPick];
                                                if (attraction != null) {
                                                    attractionName = attraction;
                                                    break insideLoop1;
                                                } else {
                                                    System.out.println("\nattraction not "
                                                            + "available");
                                                }
                                            } else {
                                                System.out.println("\ninvalid command");
                                            }
                                        } catch (Exception e) {
                                            if (command3.equals("#")) {
                                                continue insideLoop1;
                                            } else {
                                                System.out.println("\ninvalid command");
                                            }
                                        }
                                    }
                                } else {
                                    System.out.println("\nAnimal not available");
                                }
                            } catch (Exception e) {
                                if (command2.equals("#")) {
                                    continue loop;
                                } else {
                                    System.out.println("\ninvalid command");
                                }
                            }
                        }
                    } else {
                        System.out.println("\nsection not available");
                    }
                } else {
                    System.out.println("\ninvalid command");
                    continue;
                }
            } catch (Exception e) {
                if (command1.equals("#")) {
                    continue;
                } else {
                    System.out.println("\ninvalid command");
                    continue;
                }
            }

            for (Animal animal: listAnimal.get(secPick).get(typePick)) {
                if (animal.isShowable()) {
                    animalList.add(animal);
                }
            }

            SelectedAttraction attraction = new Attraction(attractionName, animalType, animalList);

            System.out.println("\nWow, one more step,");
            System.out.print("please let us know your name: ");
            name = input.nextLine();

            System.out.println("\nYeay, final check!");
            System.out.println("Here is your data, and the attraction you chose:");
            System.out.println("Name: " + name);
            System.out.println("Attractions: " + attractionName + " -> " + animalType);
            System.out.print("With: ");

            String[] performers = new String[animalList.size()];
            for (int i = 0; i < performers.length; i++) {
                performers[i] = animalList.get(i).getName();
            }
            System.out.println(String.join(", ", performers));

            System.out.print("Is the data correct? (Y/N): ");
            String confirmation1 = input.nextLine();
            if (confirmation1.equalsIgnoreCase("y")) {
                selectedAttractions.add(attraction);
                System.out.print("Thank you for your interest. " +
                        "Would you like to register to other attractions? (Y/N): ");

                String confirmation2 = input.nextLine();
                if (confirmation2.equalsIgnoreCase("n")) {
                    break;
                }
            }
        }
    }

    public static ArrayList<ArrayList<ArrayList<Animal>>> arrangeAnimal
            (ArrayList<Animal> listAnimal) {
        ArrayList<Animal> listHamster = new ArrayList<>();
        ArrayList<Animal> listLion = new ArrayList<>();
        ArrayList<Animal> listCat = new ArrayList<>();
        ArrayList<Animal> listWhale = new ArrayList<>();
        ArrayList<Animal> listEagle = new ArrayList<>();
        ArrayList<Animal> listParrot = new ArrayList<>();
        ArrayList<Animal> listSnake = new ArrayList<>();
        for(Animal animal: listAnimal) {
            switch (animal.getType().toLowerCase()) {
                case "hamster":
                    listHamster.add(animal);
                    break;
                case "lion":
                    listLion.add(animal);
                    break;
                case "cat":
                    listCat.add(animal);
                    break;
                case "parrot":
                    listParrot.add(animal);
                    break;
                case "snake":
                    listSnake.add(animal);
                    break;
                case "whale":
                    listWhale.add(animal);
                    break;
                case "eagle":
                    listEagle.add(animal);
                    break;
            }
        }
        ArrayList<ArrayList<Animal>> listMammals = new ArrayList<>();
        listMammals.add(listHamster);
        listMammals.add(listLion);
        listMammals.add(listCat);
        listMammals.add(listWhale);
        ArrayList<ArrayList<Animal>> listAves = new ArrayList<>();
        listAves.add(listEagle);
        listAves.add(listParrot);
        ArrayList<ArrayList<Animal>> listReptiles = new ArrayList<>();
        listReptiles.add(listSnake);

        ArrayList<ArrayList<ArrayList<Animal>>> arangement = new ArrayList<>();
        arangement.add(listMammals);
        arangement.add(listAves);
        arangement.add(listReptiles);

        return arangement;
    }
}