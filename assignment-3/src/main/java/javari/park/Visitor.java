package javari.park;

import javari.animal.Animal;

import java.util.List;

public class Visitor implements Registration {
    private static int idCount = 0;
    private int id;
    private String name;
    private List<SelectedAttraction> selectedAttractions;

    public Visitor(String name, List<SelectedAttraction> selectedAttractions) {
        Visitor.idCount += 1;
        this.id = Visitor.idCount;
        this.name = name;
        this.selectedAttractions = selectedAttractions;
    }

    public int getRegistrationId() {
        return this.id;
    }

    public String getVisitorName() {
        return this.name;
    }

    public String setVisitorName(String name) {
        this.name = name;
        return this.name;
    }

    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selectedAttractions;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selectedAttractions.add(selected);
        return true;
    }
}
