package javari.park;

import javari.animal.Animal;

import java.util.List;

public class Attraction implements SelectedAttraction {
    private String name;
    private String type;
    private List<Animal> performers;

    public Attraction(String name, String type, List<Animal> performers) {
        this.name = name;
        this.type = type;
        this.performers = performers;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public List<Animal> getPerformers() {
        return this.performers;
    }

    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            this.performers.add(performer);
            return true;
        } else {
            return false;
        }
    }
}