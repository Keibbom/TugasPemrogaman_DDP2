package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

public class AttractionReader extends CsvReader {
    private String[][][] listAttraction;
    public AttractionReader(Path file) throws IOException {
        super(file);

        String[] listHamster = new String[3];
        String[] listLion = new String[1];
        String[] listCat = new String[2];
        String[] listWhale = new String[2];
        String[] listEagle = new String[1];
        String[] listParrot = new String[2];
        String[] listSnake = new String[2];
        for(String i: this.getLines()) {
            String[] input = i.split(COMMA);
            String attraction = input[1];

            switch (input[0].toLowerCase()) {
                case "hamster":
                    switch (attraction) {
                        case "Dancing Animals":
                            listHamster[0] = attraction;
                            break;
                        case "Counting Masters":
                            listHamster[1] = attraction;
                            break;
                        case "Passionate Coders":
                            listHamster[2] = attraction;
                            break;
                    }
                    break;
                case "lion":
                    switch (attraction) {
                        case "Circles of Fires":
                            listLion[0] = attraction;
                            break;
                    }
                    break;
                case "cat":
                    switch (attraction) {
                        case "Dancing Animals":
                            listCat[0] = attraction;
                            break;
                        case "Passionate Coders":
                            listCat[1] = attraction;
                            break;
                    }
                    break;
                case "whale":
                    switch (attraction) {
                        case "Circles of Fires":
                            listWhale[0] = attraction;
                            break;
                        case "Counting Masters":
                            listWhale[1] = attraction;
                            break;
                    }
                    break;
                case "eagle":
                    switch (attraction) {
                        case "Circles of Fires":
                            listEagle[0] = attraction;
                            break;
                    }
                    break;
                case "parrot":
                    switch (attraction) {
                        case "Dancing Animals":
                            listParrot[0] = attraction;
                            break;
                        case "Counting Masters":
                            listParrot[1] = attraction;
                            break;
                    }
                case "snake":
                    switch (attraction) {
                        case "Dancing Animals":
                            listSnake[0] = attraction;
                            break;
                        case "Passionate Coders":
                            listSnake[1] = attraction;
                            break;
                    }
                    break;
            }
        }
        String[][] listMammals = new String[][]{listHamster, listLion, listCat, listWhale};
        String[][] listAves = new String[][]{listEagle, listParrot};
        String[][] listReptiles = new String[][]{listSnake};
        this.listAttraction = new String[][][]{listMammals, listAves, listReptiles};
    }

    public String[][][] getListAttraction() {
        return this.listAttraction;
    }

    public long countValidRecords() {
        int attraction1 = 0;
        int attraction2 = 0;
        int attraction3 = 0;
        int attraction4 = 0;

        for(String i: this.getLines()) {
            String[] input = i.split(COMMA);
            String attraction = input[1];
            switch (attraction.toLowerCase()) {
                case "circles of fires":
                    attraction1 = 1;
                    break;
                case "dancing animals":
                    attraction2 = 1;
                    break;
                case "counting masters":
                    attraction3 = 1;
                    break;
                case "passionate coders":
                    attraction4 = 1;
                    break;
            }
        }
        return attraction1 + attraction2 + attraction3 + attraction4;
    }

    public long countInvalidRecords() {
        int invalid = 0;
        for(String i: this.getLines()) {
            String[] input = i.split(COMMA);
            String attraction = input[1];
            switch (attraction.toLowerCase()) {
                case "circles of fires":
                    break;
                case "dancing animals":
                    break;
                case "counting masters":
                    break;
                case "passionate coders":
                    break;
                default:
                    invalid++;
                    break;
            }
        }
        return invalid;
    }
}
