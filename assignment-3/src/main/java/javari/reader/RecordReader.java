package javari.reader;

import javari.animal.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class RecordReader extends CsvReader{
    private ArrayList<Animal> listAnimal = new ArrayList<>();

    public RecordReader(Path file) throws IOException {
        super(file);
        for(String i: this.getLines()) {
            String[] input = i.split(COMMA);
            int id = Integer.parseInt(input[0]);
            String type = input[1];
            String name = input[2];
            Gender gender = Gender.parseGender(input[3]);
            double length = Double.parseDouble(input[4]);
            double weight = Double.parseDouble(input[5]);
            String specialCondition = input[6];
            Condition condition = Condition.parseCondition(input[7]);
            switch (type.toLowerCase()) {
                case "hamster":
                    Animal hamster = new Hamster(id, type, name, gender,
                            length, weight, specialCondition, condition);
                    this.listAnimal.add(hamster);
                    break;
                case "lion":
                    Animal lion = new Lion(id, type, name, gender,
                            length, weight, specialCondition, condition);
                    this.listAnimal.add(lion);
                    break;
                case "cat":
                    Animal cat = new Cat(id, type, name, gender,
                            length, weight, specialCondition, condition);
                    this.listAnimal.add(cat);
                    break;
                case "parrot":
                    Animal parrot = new Parrot(id, type, name, gender,
                            length, weight, specialCondition, condition);
                    this.listAnimal.add(parrot);
                    break;
                case "snake":
                    Animal snake = new Snake(id, type, name, gender,
                            length, weight, specialCondition, condition);
                    this.listAnimal.add(snake);
                    break;
                case "whale":
                    Animal whale = new Whale(id, type, name, gender,
                            length, weight, specialCondition, condition);
                    this.listAnimal.add(whale);
                    break;
                case "eagle":
                    Animal eagle = new Eagle(id, type, name, gender,
                            length, weight, specialCondition, condition);
                    this.listAnimal.add(eagle);
                    break;
            }
        }
    }

    public ArrayList<Animal> getAnimalList() {
        return this.listAnimal;
    }

    public long countValidRecords() {
        return this.listAnimal.size();
    }

    public long countInvalidRecords() {
        return this.getLines().size() - this.listAnimal.size();
    }
}