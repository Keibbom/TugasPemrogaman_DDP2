package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

public class SectionReader extends CsvReader{
    private String[] listSection;
    public SectionReader(Path file) throws IOException {
        super(file);
        this.listSection = new String[3];

        for(String i: this.getLines()) {
            String[] input = i.split(COMMA);
            String section = input[2];
            switch (section.toLowerCase()) {
                case "explore the mammals":
                    this.listSection[0] = section;
                    break;
                case "world of aves":
                    this.listSection[1] = section;
                    break;
                case "reptillian kingdom":
                    this.listSection[2] = section;
                    break;
            }
        }
    }

    public String[] getListSection() {
        return this.listSection;
    }

    public long countValidRecords() {
        int valid = 0;
        if (this.listSection[0] != null) valid += 1;
        if (this.listSection[1] != null) valid += 1;
        if (this.listSection[2] != null) valid += 1;
        return valid;
    }

    public long countInvalidRecords() {
        int invalid = 0;
        for(String i: this.getLines()) {
            String[] input = i.split(COMMA);
            String section = input[2];
            switch (section.toLowerCase()) {
                case "explore the mammals":
                    break;
                case "world of aves":
                    break;
                case "reptillian kingdom":
                    break;
                default:
                    invalid++;
                    break;
            }
        }
        return invalid;
    }
}
