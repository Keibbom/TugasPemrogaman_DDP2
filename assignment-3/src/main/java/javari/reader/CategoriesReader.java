package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

public class CategoriesReader extends CsvReader {
    private String[][] listCategories;

    public CategoriesReader(Path file) throws IOException {
        super(file);
        String[] listMammals = new String[4];
        String[] listAves = new String[4];
        String[] listReptiles = new String[4];
        for(String i: this.getLines()) {
            String[] input = i.split(COMMA);
            String categories = input[1];
            switch (categories.toLowerCase()) {
                case "mammals":
                    if (input[0].equalsIgnoreCase("Hamster")) listMammals[0] = input[0];
                    if (input[0].equalsIgnoreCase("Lion")) listMammals[1] = input[0];
                    if (input[0].equalsIgnoreCase("Cat")) listMammals[2] = input[0];
                    if (input[0].equalsIgnoreCase("Whale")) listMammals[3] = input[0];
                    break;
                case "aves":
                    if (input[0].equalsIgnoreCase("Eagle")) listAves[0] = input[0];
                    if (input[0].equalsIgnoreCase("Parrot")) listAves[1] = input[0];
                    break;
                case "reptiles":
                    if (input[0].equalsIgnoreCase("Snake")) listReptiles[0] = input[0];
                    break;
            }
        }
        this.listCategories = new String[][]{listMammals, listAves, listReptiles};
    }

    public String[][] getListCategories() {
        return this.listCategories;
    }

    public long countValidRecords(){
        int valid = 0;
        if (this.listCategories[0] != null) valid += 1;
        if (this.listCategories[1] != null) valid += 1;
        if (this.listCategories[2] != null) valid += 1;
        return valid;
    }

    public long countInvalidRecords() {
        int invalid = 0;
        for (String i : this.getLines()) {
            String[] input = i.split(COMMA);
            String categories = input[1];
            switch (categories.toLowerCase()) {
                case "mammals":
                    break;
                case "aves":
                    break;
                case "reptiles":
                    break;
                default:
                    invalid++;
                    break;
            }
        }
        return invalid;
    }
}