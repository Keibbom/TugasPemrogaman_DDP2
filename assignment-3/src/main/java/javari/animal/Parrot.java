package javari.animal;

public class Parrot extends Animal {
    private boolean isLayingEgg;

    public Parrot(Integer id, String type, String name, Gender gender, double length,
                  double weight, String layingEgg, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isLayingEgg = layingEgg.toLowerCase().equals("LayingEgg");
    }

    protected boolean specificCondition() {
        return !this.isLayingEgg;
    }
}
