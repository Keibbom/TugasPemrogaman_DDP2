package javari.animal;

public class Cat extends Animal{
    private boolean isPregnant;

    public Cat(Integer id, String type, String name, Gender gender,
               double length, double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isPregnant = pregnant.toLowerCase().equals("pregnant");
    }

    protected boolean specificCondition() {
        return !this.isPregnant;
    }
}
