package javari.animal;

public class Whale extends Animal {
    private boolean isPregnant;

    public Whale(Integer id, String type, String name, Gender gender,
               double length, double weight, String pregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isPregnant = pregnant.toLowerCase().equals("pregnant");
    }

    public boolean specificCondition() {
        return !this.isPregnant;
    }
}
