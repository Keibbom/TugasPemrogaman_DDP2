package javari.animal;

public class Snake extends Animal{
    private Boolean isTame;

    public Snake(Integer id, String type, String name, Gender gender, double length,
                 double weight, String wildness, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isTame = wildness.toLowerCase().equals("tame");
    }

    protected boolean specificCondition() {
        return this.isTame;
    }
}
