public class Binatang {
    protected String nama;
    protected int panjang;
    protected boolean liar;
    protected Kandang kandang;

    public Binatang(String nama, int panjang, boolean liar) {
        this.nama = nama;
        this.panjang = panjang;
        this.liar = liar;
        this.kandang = new Kandang(panjang, liar);
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getPanjang() {
        return panjang;
    }

    public boolean liar() {
        return liar;
    }

    public Kandang getKandang() {
        return kandang;
    }

    public void nothing() {
        System.out.println("You do nothing...");
        System.out.println("Back to the office!");
    }
}