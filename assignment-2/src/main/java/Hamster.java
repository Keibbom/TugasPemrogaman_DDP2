public class Hamster extends Binatang {

    public Hamster(String nama, int panjang) {
        super(nama, panjang, false);
    }

    public void kunyah() {
        System.out.println(this.nama + " makes a voice: ngkkrit.. ngkkrrriiit");
        System.out.println("Back to the office!");
    }

    public void lari() {
        System.out.println(this.nama + " makes a voice: trrr…. trrr...");
        System.out.println("Back to the office!");
    }
}