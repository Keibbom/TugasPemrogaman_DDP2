public class Kandang {
    private String tipe;
    private int lebar;
    private int panjang;
    private String lokasi;

    public Kandang(int panjangHewan, boolean liar) {
        if(liar) {
            this.lokasi = "outdoor";
            if (panjangHewan < 75) {
                this.tipe = "A";
                this.lebar = 120;
                this.panjang = 120;
            } else if (panjangHewan >= 75 && panjangHewan <= 90) {
                this.tipe = "B";
                this.lebar = 120;
                this.panjang = 150;
            } else {
                this.tipe = "C";
                this.lebar = 120;
                this.panjang = 180;
            }
        } else {
            this.lokasi = "indoor";
            if (panjangHewan < 45) {
                this.tipe = "A";
                this.lebar = 60;
                this.panjang = 60;
            } else if (panjangHewan >= 45 && panjangHewan <= 60) {
                this.tipe = "B";
                this.lebar = 60;
                this.panjang = 90;
            } else {
                this.tipe = "C";
                this.lebar = 60;
                this.panjang = 120;
            }
        }


    }

    public String getTipe() {
        return tipe;
    }

    public String getLokasi() {
        return lokasi;
    }

    public static Binatang[][] aturKandang(Binatang[] listBinatang) {
        int level1 = listBinatang.length / 3;
        int level2 = listBinatang.length / 3;
        int level3 = listBinatang.length / 3;

        if (listBinatang.length > 3) {
            if (listBinatang.length % 3 == 2) {
                level3 += 1;
                level2 += 1;
            } else if (listBinatang.length % 3 == 1) {
                level3 += 1;
            }
        } else {
            if (listBinatang.length % 3 == 2) {
                level1 += 1;
                level2 += 1;
            } else if (listBinatang.length % 3 == 1) {
                level1 += 1;
            }
        }

        Binatang[] listLevel1 = new Binatang[level1];
        Binatang[] listLevel2 = new Binatang[level2];
        Binatang[] listLevel3 = new Binatang[level3];
        for (int i = 0; i < listBinatang.length; i++) {
            if (i < level1) {
                listLevel1[i] = listBinatang[i];
            } else if (i < level1 + level2 ) {
                listLevel2[i - level1] = listBinatang[i];
            } else if (i < level1 + level2 + level3){
                listLevel3[i - level1 - level2] = listBinatang[i];
            }
        }
        Binatang[][] kandang = new Binatang[][]{listLevel3, listLevel2, listLevel1};
        return kandang;
    }

    public static Binatang[][] aturUlang(Binatang[][] listBinatang) {
        Binatang[] listLevel1 = listBinatang[0].clone();
        Binatang[] listLevel2 = listBinatang[2].clone();
        Binatang[] listLevel3 = listBinatang[1].clone();

        for (int i = 0; i < listLevel1.length / 2; i++) {
            Binatang temp = listLevel1[i];
            listLevel1[i] = listLevel1[listLevel1.length - 1 - i];
            listLevel1[listLevel1.length - 1 - i] = temp;
        }

        for (int i = 0; i < listLevel2.length / 2; i++) {
            Binatang temp = listLevel2[i];
            listLevel2[i] = listLevel2[listLevel2.length - 1 - i];
            listLevel2[listLevel2.length - 1 - i] = temp;
        }

        for (int i = 0; i < listLevel3.length / 2; i++) {
            Binatang temp = listLevel3[i];
            listLevel3[i] = listLevel3[listLevel3.length - 1 - i];
            listLevel3[listLevel3.length - 1 - i] = temp;
        }
        Binatang[][] kandang = new Binatang[][]{listLevel3, listLevel2, listLevel1};
        return kandang;
    }

    public static void printKandang(Binatang[][] kandang) {
        System.out.print("level 3: ");
        for (int i = 0; i < kandang[0].length; i++){
            System.out.print(kandang[0][i].getNama()
                    + " (" + kandang[0][i].getPanjang()
                    + " - " + kandang[0][i].getKandang().getTipe() + "), ");
        }
        System.out.print("\nlevel 2: ");
        for (int i = 0; i < kandang[1].length; i++){
            System.out.print(kandang[1][i].getNama()
                    + " (" + kandang[1][i].getPanjang()
                    + " - " + kandang[1][i].getKandang().getTipe() + "), ");
        }
        System.out.print("\nlevel 1: ");
        for (int i = 0; i < kandang[2].length; i++){
            System.out.print(kandang[2][i].getNama()
                    + " (" + kandang[2][i].getPanjang()
                    + " - " + kandang[2][i].getKandang().getTipe() + "), ");
        }
    }
}
