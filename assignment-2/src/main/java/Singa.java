public class Singa extends Binatang {

    public Singa(String nama, int panjang) {
        super(nama, panjang, true);
    }

    public void berburu() {
        System.out.println("Lion is hunting..");
        System.out.println(this.nama + " makes a voice: err...!");
        System.out.println("Back to the office!");
    }

    public void sikat() {
        System.out.println("Clean the lion’s mane..");
        System.out.println(this.nama + " makes a voice: Hauhhmm!");
        System.out.println("Back to the office!");
    }

    public void ganggu() {
        System.out.println(this.nama + " makes a voice: HAUHHMM!");
        System.out.println("Back to the office!");
    }
}