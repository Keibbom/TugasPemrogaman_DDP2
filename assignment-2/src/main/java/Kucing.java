import java.util.Random;

public class Kucing extends Binatang {
    private String[] suaraPeluk;

    public Kucing(String nama, int panjang) {
        super(nama, panjang, false);
        this.suaraPeluk = new String[] {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    }

    public void sikat() {
        System.out.println("Time to clean " + this.nama + "'s fur");
        System.out.println(this.nama + " makes a voice: Nyaaan...");
        System.out.println("Back to the office!");
    }

    public void peluk() {
        Random random = new Random();
        String suara = this.suaraPeluk[random.nextInt(4)];
        System.out.println(this.nama + " makes a voice: " + suara);
        System.out.println("Back to the office!");
    }
}