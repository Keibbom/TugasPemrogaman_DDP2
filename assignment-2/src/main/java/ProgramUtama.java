import java.util.ArrayList;
import java.util.Scanner;

public class ProgramUtama {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari Park!\n"
                + "Input the number of animals");

        System.out.print("cat: ");
        int jumlahKucing = Integer.parseInt(input.nextLine());
        Kucing[] listKucing = new Kucing[jumlahKucing];
        if (jumlahKucing > 0) {
            System.out.println("Provide the information of cat(s):");
            String[] inputKucing = input.nextLine().split("[|,]");
            for (int i = 0, j = 0; j < jumlahKucing; j++) {
                String nama = inputKucing[i];
                int panjang = Integer.parseInt(inputKucing[i+1]);
                Kucing kucing = new Kucing(nama, panjang);
                listKucing[j] = kucing;
                i += 2;
            }
        }

        System.out.print("lion: ");
        int jumlahSinga = Integer.parseInt(input.nextLine());
        Singa[] listSinga = new Singa[jumlahSinga];
        if (jumlahSinga > 0) {
            System.out.println("Provide the information of lion(s):");
            String[] inputSinga = input.nextLine().split("[|,]");
            for (int i = 0, j = 0; j < jumlahSinga; j++) {
                String nama = inputSinga[i];
                int panjang = Integer.parseInt(inputSinga[i + 1]);
                Singa singa = new Singa(nama, panjang);
                listSinga[j] = singa;
                i += 2;
            }
        }

        System.out.print("eagle: ");
        int jumlahElang = Integer.parseInt(input.nextLine());
        Elang[] listElang = new Elang[jumlahElang];
        if (jumlahElang > 0) {
            System.out.println("Provide the information of eagle(s):");
            String[] inputElang = input.nextLine().split("[|,]");
            for (int i = 0, j = 0; j < jumlahElang; j++) {
                String nama = inputElang[i];
                int panjang = Integer.parseInt(inputElang[i + 1]);
                Elang elang = new Elang(nama, panjang);
                listElang[j] = elang;
                i += 2;
            }
        }

        System.out.print("parrot: ");
        int jumlahBeo = Integer.parseInt(input.nextLine());
        Beo[] listBeo = new Beo[jumlahBeo];
        if (jumlahBeo > 0) {
            System.out.println("Provide the information of parrot(s):");
            String[] inputBeo = input.nextLine().split("[|,]");
            for (int i = 0, j = 0; j < jumlahBeo; j++) {
                String nama = inputBeo[i];
                int panjang = Integer.parseInt(inputBeo[i + 1]);
                Beo beo = new Beo(nama, panjang);
                listBeo[j] = beo;
                i += 2;
            }
        }

        System.out.print("hamster: ");
        int jumlahHamster = Integer.parseInt(input.nextLine());
        Hamster[] listHamster = new Hamster[jumlahHamster];
        if (jumlahHamster > 0) {
            System.out.println("Provide the information of hamster(s):");
            String[] inputHamster = input.nextLine().split("[|,]");
            for (int i = 0, j = 0; j < jumlahHamster; j++) {
                String nama = inputHamster[i];
                int panjang = Integer.parseInt(inputHamster[i + 1]);
                Hamster hamster = new Hamster(nama, panjang);
                listHamster[j] = hamster;
                i += 2;
            }
        }
        System.out.println("Animals have been successfully recorded!\n\n" +
							"=============================================");

        Binatang[][] listBinatang = new Binatang[][]{listKucing, listSinga, listElang, listBeo, listHamster};

        for (int i = 0; i < listBinatang.length; i++) {
            if (listBinatang[i].length > 0) {
                System.out.println("Cage arrangement:");
                System.out.println("location: " + listBinatang[i][0].getKandang().getLokasi());
                Binatang[][] aturKandang = Kandang.aturKandang(listBinatang[i]);
                Binatang[][] aturUlang = Kandang.aturUlang(aturKandang);
                Kandang.printKandang(aturKandang);
                System.out.println("\n");
                System.out.println("After rearrangement...");
                Kandang.printKandang(aturUlang);
                System.out.println("\n");
            }
        }

        System.out.println("NUMBER OF ANIMALS:\n"
                + "cat:" + listKucing.length + "\n"
                + "lion:" + listSinga.length + "\n"
                + "parrot:" + listBeo.length + "\n"
                + "eagle:" + listElang.length + "\n"
                + "hamster:" + listHamster.length + "\n");
        System.out.println("=============================================");

        while (true) {
            System.out.print("Which animal you want to visit?\n");
            System.out.print("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)\n");
            String perintah = input.nextLine();
            if (perintah.equals("1")) {
                System.out.print("Mention the name of cat you want to visit: ");
                String nama = input.nextLine();
                boolean kucingKosong = true;
                if (jumlahKucing > 0) {
                    for (Kucing i : listKucing) {
                        if (nama.equals(i.getNama())) {
                            System.out.println("You are visiting " + nama + " (cat) now, what would you like to do?");
                            System.out.println("1: Brush the fur 2: Cuddle");
                            int perintahKucing = Integer.parseInt(input.nextLine());
                            if (perintahKucing == 1) {
                                i.sikat();
                            } else if (perintahKucing == 2) {
                                i.peluk();
                            } else {
                                i.nothing();
                            }
                            System.out.println();
                            kucingKosong = false;
                        }
                    }
                    if (kucingKosong) {
                        System.out.println("There is no cat with that name! Back to the office!");
                        System.out.println();
                    }
                } else {
                    System.out.println("There is no cat in the office! Back to the office!");
                }
            } else if (perintah.equals("2")) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String nama = input.nextLine();
                boolean elangKosong = true;
                if (jumlahElang > 0) {
                    for (Elang i : listElang) {
                        if (nama.equals(i.getNama())) {
                            System.out.println("You are visiting " + nama + " (eagle) now, what would you like to do?");
                            System.out.println("1: Order to fly");
                            int perintahElang = Integer.parseInt(input.nextLine());
                            if (perintahElang == 1) {
                                i.terbang();
                            } else {
                                i.nothing();
                            }
                            System.out.println();
                            elangKosong = false;
                        }
                    }
                    if (elangKosong) {
                        System.out.println("There is no eagle with that name! Back to the office!");
                        System.out.println();
                    }
                } else {
                    System.out.println("There is no eagle in the office! Back to the office!");
                }

            } else if (perintah.equals("3")) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String nama = input.nextLine();
                boolean hamsterKosong = true;
                if (jumlahHamster > 0) {
                    for (Hamster i : listHamster) {
                        if (nama.equals(i.getNama())) {
                            System.out.println("You are visiting " + nama + " (hamster) now, what would you like to do?");
                            System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                            int perintahHamster = Integer.parseInt(input.nextLine());
                            if (perintahHamster == 1) {
                                i.kunyah();
                            } else if (perintahHamster == 2) {
                                i.lari();
                            } else {
                                i.nothing();
                            }
                            System.out.println();
                            hamsterKosong = false;
                        }
                    }
                    if (hamsterKosong) {
                        System.out.println("There is no hamster with that name! Back to the office!");
                        System.out.println();
                    }
                } else {
                    System.out.println("There is no hamster in the office! Back to the office!");
                }

            } else if (perintah.equals("4")) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String nama = input.nextLine();
                boolean beoKosong = true;
                if (jumlahBeo > 0) {
                    for (Beo i : listBeo) {
                        if (nama.equals(i.getNama())) {
                            System.out.println("You are visiting " + nama + " (parrot) now, what would you like to do?");
                            System.out.println("1: Order to fly 2: Do conversation");
                            int perintahBeo = Integer.parseInt(input.nextLine());
                            if (perintahBeo == 1) {
                                i.terbang();
                            } else if (perintahBeo == 2) {
                                System.out.print("You say: ");
                                String kata = input.nextLine();
                                i.bicara(kata);
                            } else {
                                i.nothing();
                            }
                            System.out.println();
                            beoKosong = false;
                        }
                    }
                    if (beoKosong) {
                        System.out.println("There is no parrot with that name! Back to the office!");
                        System.out.println();
                    }
                } else {
                    System.out.println("There is no parrot in the office! Back to the office!");
                }

            } else if (perintah.equals("5")) {
                System.out.print("Mention the name of lion you want to visit: ");
                String nama = input.nextLine();
                boolean singaKosong = true;
                if (jumlahSinga > 0) {
                    for (Singa i : listSinga) {
                        if (nama.equals(i.getNama())) {
                            System.out.println("You are visiting " + nama + " (lion) now, what would you like to do?");
                            System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
                            int perintahSinga = Integer.parseInt(input.nextLine());
                            if (perintahSinga == 1) {
                                i.berburu();
                            } else if (perintahSinga == 2) {
                                i.sikat();
                            } else if (perintahSinga == 3) {
                                i.ganggu();
                            } else {
                                i.nothing();
                            }
                            System.out.println();
                            singaKosong = false;
                        }
                    }
                    if (singaKosong) {
                        System.out.println("There is no lion with that name! Back to the office!");
                        System.out.println();
                    }
                } else {
                    System.out.println("There is no lion in the office! Back to the office!");
                }

            } else if (perintah.equals("99")) {
                break;
            } else {
                System.out.println("Wrong command! Back to the office!");
            }
        }
    }

}
