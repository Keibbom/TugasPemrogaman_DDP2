public class Elang extends Binatang {

    public Elang(String nama, int panjang) {
        super(nama, panjang, true);
    }

    public void terbang() {
        System.out.println(this.nama + " makes a voice: kwaakk…");
        System.out.println("You hurt!");
        System.out.println("Back to the office!");
    }
}