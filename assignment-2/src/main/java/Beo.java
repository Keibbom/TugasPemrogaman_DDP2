public class Beo extends Binatang {

    public Beo(String nama, int panjang) {
        super(nama, panjang, false);
    }

    public void terbang() {
        System.out.println("Parrot " + this.nama +" flies!");
        System.out.println(this.nama + " makes a voice: FLYYYY…..");
        System.out.println("Back to the office!");
    }

    public void bicara(String kata) {
        String suara = kata.toUpperCase();
        System.out.println(this.nama + " says: " + suara);
        System.out.println("Back to the office!");
    }

    public void nothing() {
        System.out.println(this.nama + " says: HM?");
        System.out.println("Back to the office!");
    }
}