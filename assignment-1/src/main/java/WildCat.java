//Class Kucing

public class WildCat{
	//Atribut
	String name;
	double weight;
	double length;
	
	//Konstruktor
	public WildCat(String name, double weight, double length){
		this.name = name;
		this.weight = weight;
		this.length = length;
	}
	
	//Method
	public double computeMassIndex(){
		return weight/Math.pow(length/100, 2);
	}
}