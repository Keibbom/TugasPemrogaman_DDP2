//Class Kereta

public class TrainCar{
	//Konstanta
	public static final double EMPTY_WEIGHT = 20;
	
	//Atribut
	WildCat kucing;
	TrainCar keretaSelanjutnya;
	
	//Konstruktor
	public TrainCar(WildCat kucing){
		this.kucing = kucing;
	}
	public TrainCar(WildCat kucing, TrainCar keretaSelanjutnya){
		this.kucing = kucing;
		this.keretaSelanjutnya = keretaSelanjutnya;
	}
	
	//Method untuk Menghitung Berat Total
	public double computeTotalWeight(){
		if (this.keretaSelanjutnya == null){
			return EMPTY_WEIGHT + kucing.weight;
		}else{
			return EMPTY_WEIGHT + kucing.weight + keretaSelanjutnya.computeTotalWeight();
		}
	}
	
	//Method untuk Menghitung Indeks Massa Total Kucing
	public double computeTotalMassIndex(){
		if (this.keretaSelanjutnya == null){
			return kucing.computeMassIndex();
		}else{
			return kucing.computeMassIndex() + keretaSelanjutnya.computeTotalMassIndex();
		}
	}
	
	//Method untuk Print Rangkaian Kereta
	public void printCar(){
		if (keretaSelanjutnya == null){
			System.out.print("(" + kucing.name + ")");
		} else {
			System.out.print("(" + kucing.name + ")--");
			keretaSelanjutnya.printCar();
		}
	}

}