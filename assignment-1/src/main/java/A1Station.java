//Program utama Javari Park

import java.util.Scanner;
import java.util.ArrayList;

public class A1Station {
	//Konstanta
	private static final double THRESHOLD = 250;
	
	//Main
    public static void main(String[] args) {
		//Pembuka + Meminta Inputan Jumlah Kucing
		Scanner input = new Scanner(System.in);
		System.out.print("WELCOME TO JAVARI PARK\n" + 
			"--------------------\n" +
			"Number of Cats : "); 
		String jumlahKucing = input.next();
		int jumlahKucingInt = Integer.parseInt(jumlahKucing);
		
		//Membuat Objek Kucing Sebanyak Jumlah Kucing
		ArrayList<WildCat> kumpulanKucing = new ArrayList<WildCat>();
		for(int i = 0; i < jumlahKucingInt; i++){
			System.out.print(">> ");
			String spekKucing = input.next();
			String splitKucing[] = spekKucing.split(",");
			
			String namaKucing = splitKucing[0];
			double beratKucing = Double.parseDouble(splitKucing[1]);
			double panjangKucing = Double.parseDouble(splitKucing[2]);
			
			WildCat kucing = new WildCat(namaKucing, beratKucing, panjangKucing);
			kumpulanKucing.add(kucing);
		}
		
		//Membuat Objek Kereta
		while(kumpulanKucing.size() != 0){
			TrainCar keretaKucing = null;
			int gerbong = 0;
			while(kumpulanKucing.size() != 0){
				if(keretaKucing == null){
					keretaKucing = new TrainCar(kumpulanKucing.get(0));	
					kumpulanKucing.remove(0);
					gerbong += 1;
				} else if(keretaKucing.computeTotalWeight() <= THRESHOLD){
					keretaKucing = new TrainCar(kumpulanKucing.get(0), keretaKucing);
					kumpulanKucing.remove(0);
					gerbong += 1;
				} else{
					break;
				}
			}
			//Menentukan Kategori Indeks Massa
			String kategori = "";
			double rataIndeks = keretaKucing.computeTotalMassIndex() / gerbong;
			if(rataIndeks < 18.5){
				kategori = "underweight";
			} else if(18.5 <= rataIndeks && rataIndeks < 25){
				kategori = "normal";
			} else if(25 <= rataIndeks && rataIndeks <30){
				kategori = "overweight";
			} else {
				kategori = "obese";
			}
			
			//Output
			System.out.println("--------------------");
			System.out.println("The train departs to Javari Park");
			System.out.print("[LOCO]<--"); keretaKucing.printCar(); System.out.print("\n");
			System.out.println("Average mass index of all cats: " + String.format("%.2f", rataIndeks));
			System.out.println("In average, the cats in the train are *" + kategori + "*");
		}
		
		
		
			
		



    }
}
