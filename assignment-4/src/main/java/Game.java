import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Game{
    public static void main(String[] args){
        Board b = new Board();
		b.setPreferredSize(new Dimension(600,700));
        b.setLocation(300, 20);
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setVisible(true);
    }   
}