import javax.imageio.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;

@SuppressWarnings("serial")
public class Card extends JButton{
	private ImageIcon frontImage;
	private ImageIcon backImage;
    private int id;
    private boolean matched = false;
	
	public Card(int val){
		try{
			Image back = ImageIO.read(new File("img\\default\\avengers.png"));
			Image back1 = back.getScaledInstance(90,90, java.awt.Image.SCALE_SMOOTH);
			this.backImage = new ImageIcon(back1);
			this.setIcon(backImage);
			
			Image front = ImageIO.read(new File("img\\content\\" + val + ".png"));
			Image front1 = front.getScaledInstance(90,90, java.awt.Image.SCALE_SMOOTH);
			this.frontImage = new ImageIcon(front1);
			
			this.id = val;
		}
		catch(Exception e){
			System.out.println("Can't find image!");
		}
	}

    public int getId(){
        return this.id;
    }

    public void setMatched(boolean matched){
        this.matched = matched;
    }

    public boolean getMatched(){
        return this.matched;
	}
	
	public void flipImage(){
		if(this.getIcon().equals(backImage)){
			this.setIcon(frontImage);
		} else{
			this.setIcon(backImage);
		}
	}
}