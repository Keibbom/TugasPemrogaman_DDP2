import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

@SuppressWarnings("serial")
public class Board extends JFrame{
    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
	private int tries;
	private JLabel score;
	private Container pane;
	private JPanel mainPanel;

    public Board(){
		makeGame();

        //set up the board itself
        pane = getContentPane();
        pane.setLayout(new BorderLayout());
        setTitle("Remember the Avengers");
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(6,6));
		for (Card c : cards){
			panel.add(c);
		}
		panel.setPreferredSize(new Dimension(600, 600));
		mainPanel.add(panel, BorderLayout.PAGE_START);
		
		JPanel setting = new JPanel();
		setting.setLayout(new GridBagLayout());
		GridBagConstraints con = new GridBagConstraints();
		con.fill = GridBagConstraints.VERTICAL;
		con.anchor = GridBagConstraints.CENTER;
		con.ipadx = 30;
		con.ipady = 1;
		con.gridx = 0;
		con.gridy = 0;
		
		//Button
		JButton playAgain = new JButton("Play Again ?");
		playAgain.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						panel.removeAll();
						makeGame();
						for (Card c : cards){
							panel.add(c);
						}
						mainPanel.add(panel, BorderLayout.PAGE_START);
						mainPanel.revalidate();
						mainPanel.repaint();
					}});
		JButton exit = new JButton("Exit");
		exit.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						System.exit(0);
					}});
					
		setting.add(playAgain, con);
		con.gridx = 1;
		setting.add(exit, con);
		setting.setPreferredSize(new Dimension(30, 30));
		mainPanel.add(setting, BorderLayout.CENTER);
		
		score = new JLabel("Number of tries : " + tries, SwingConstants.CENTER);
		score.setPreferredSize(new Dimension(30, 30));
		mainPanel.add(score, BorderLayout.PAGE_END);
		
		pane.add(mainPanel);
    }

    public void doTurn(){
        if (c1 == null && c2 == null){
            c1 = selectedCard;
            c1.flipImage();
        }

        if (c1 != null && c1 != selectedCard && c2 == null){
            c2 = selectedCard;
            c2.flipImage();
            t.start();
        }
    }

    public void checkCards(){
        if (c1.getId() == c2.getId()){//match condition
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()){
                JOptionPane.showMessageDialog(this, "You win!");
                System.exit(0);
            }
        }

        else{
            c1.flipImage(); //'hides' text
            c2.flipImage();
        }
        c1 = null; //reset c1 and c2
        c2 = null;
		tries++;
		mainPanel.remove(score);
		score = new JLabel("Number of tries : " + tries, SwingConstants.CENTER);
		score.setPreferredSize(new Dimension(30, 30));
		mainPanel.add(score, BorderLayout.PAGE_END);
		mainPanel.revalidate();
		mainPanel.repaint();
    }

    public boolean isGameWon(){
        for(Card c: this.cards){
            if (c.getMatched() == false){
                return false;
            }
        }
        return true;
    }
	
	public void makeGame(){
		int tries = 0;
        int pairs = 19;
        List<Card> cardsList = new ArrayList<Card>();
        List<Integer> cardVals = new ArrayList<Integer>();

        for (int i = 1; i < pairs; i++){
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);

        for (int val : cardVals){
            Card c = new Card(val);
            c.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae){
                    selectedCard = c;
                    doTurn();
                }
            });
            cardsList.add(c);
        }
        this.cards = cardsList;
        //set up the timer
        t = new Timer(750, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                checkCards();
            }
        });

        t.setRepeats(false);
	}

}